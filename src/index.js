import React from 'react';
import ReactDOM from 'react-dom'
import axios from 'axios';
import moment from 'moment';
import './index.css';


function DataColumn(props){
  // Making Array of column data
  var row =[];
  for(var i = 0; i < 7; i++){
    const temp = props.data.filter(persons => persons[0] === i)
    row.push(
      <td>
        { temp.map(eachData =>
          <div>
            <img alt={eachData[1]} src={eachData[2]}></img>
            <a>{eachData[1]}</a>
          </div>
        )}
      </td>
    );
  }
    return <tr>{row}</tr>;
}

export default class PersonList extends React.Component {
  state = {
    persons: [],
  }

  componentDidMount() {
    axios.get(`http://uinames.com/api/?ext&amount=25`)
      .then(res => {
        // Retrieve data and create array with only necessary data
        const persons = res.data.map((person) => [moment(person.birthday.mdy, "MM-DD-YYYY").day(),person.name,person.photo]);
        this.setState({ persons });
      })
  }

  render() {
    return (
      <div>
      <table>
      <tbody>
        <tr>
          <th>Sun</th>
          <th>Mon</th>
          <th>Tue</th>
          <th>Wed</th>
          <th>Thu</th>
          <th>Fri</th>
          <th>Sat</th>
        </tr>
        <DataColumn data={this.state.persons}/>
        </tbody>
      </table>
      </div>

    )
  }
}

// ========================================

ReactDOM.render(
  <PersonList />,
  document.getElementById('root')
);
